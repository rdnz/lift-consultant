{
  description = "template";

  # inputs.nixpkgs.url = "github:nixos/nixpkgs/7c9cc5a6e5d38010801741ac830a3f8fd667a7a0"; # nixos-unstable-2023-10-19
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem
      (system:
        with nixpkgs.legacyPackages.${system};
        {
          devShells.default =
            mkShell {
              packages =
                [
                  haskell.compiler.ghc924 # 946
                  cabal-install
                  haskellPackages.cabal-fmt
                  ghcid
                  ormolu
                  # haskell-language-server
                ];
            };
        }
      );
}
