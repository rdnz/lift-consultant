{-# options_ghc
  -Wno-redundant-constraints
#-}

module Std
  (
    module Std,
    module Partial,
    module Relude,
    module Data.Foldable1,
    module Control.Exception.Safe,
  )
  where

import Partial (Partial, error, unsafePartial)
import Control.Exception.Safe
import Data.Foldable1
import Data.List.NonEmpty qualified as N
import Path (File, Path, toFilePath)
import Relude hiding (intercalate, some, head, last, error)

readFileUtf8 :: (MonadIO m) => Path b File -> m Text
readFileUtf8 =
  (=<<) (either (liftIO . throw) pure) .
  fmap decodeUtf8Strict .
  readFileBS .
  toFilePath

writeFileUtf8 :: (MonadIO m) => Path b File -> Text -> m ()
writeFileUtf8 path = writeFileBS (toFilePath path) . encodeUtf8

infixr 9 .:
(.:) :: (c -> d) -> (a -> b -> c) -> a -> b -> d
(.:) = (.) . (.)

infixl 4 <<*>>
(<<*>>) ::
  (Applicative f, Applicative g) => f (g (a -> b)) -> f (g a) -> f (g b)
(<<*>>) = liftA2 (<*>)

bind2 :: Monad m => (a -> b -> m c) -> m a -> m b -> m c
bind2 f a b = uncurry f =<< liftA2 (,) a b

iterateFinite :: (a -> Either b a) -> a -> ([a], b)
iterateFinite f =
  go
  where
    go aOld
      | Right aNew <- f aOld = first (aNew :) (go aNew)
      | Left b <- f aOld = ([], b)

-- | fails if the second argument is 'minBound'
range :: (Partial, Enum a) => a -> a -> [a]
range lower upper = enumFromTo lower (pred upper)

some :: Alternative f => f a -> f (NonEmpty a)
some v = (:|) <$> v <*> many v

sortNonEmptyOn :: Ord b => (a -> b) -> NonEmpty a -> NonEmpty a
sortNonEmptyOn f =
  fmap snd .
  N.sortBy (comparing fst) .
  fmap (\a -> let b = f a in b `seq` (b, a))

intercalate :: (Monoid a, Foldable t) => a -> t a -> a
intercalate a = fold . intersperse a . toList

compose ::
  forall t a.
  (Foldable t, Coercible (t (a -> a)) (t (Endo a))) =>
  t (a -> a) -> a -> a
compose = coerce @(t (Endo a) -> Endo a) fold

infix 4 !=
(!=) :: (Eq a) => a -> a -> Bool
(!=) = (/=)
