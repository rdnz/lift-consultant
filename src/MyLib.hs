module MyLib where

import Data.Sequence (Seq ((:<|), (:|>)))
import Data.Sequence qualified as S
import Relude hiding (Down, State, state)

-- state
data State = State
  { -- | highest storey below or equal to the lift's position
    storey :: !Integer,
    -- | storeys that users either requested for departure or
    -- destination. more recent requests are further left.
    requests :: !(Seq Integer)
  }

-- instructions
data ShouldStop = StopAndStartBoardingCountdown | NoStop
data Move = Down | Up | None
data StartBoardingCountdown = StartBoardingCountdown

-- update on events
updateOnDeparturingStorey :: State -> (State, Move)
updateOnDeparturingStorey (State storey requests@(_ :|> requestNext))
  | requestNext < storey = (State (storey - 1) requests, Down)
  | storey < requestNext = (State storey requests, Up)
updateOnDeparturingStorey liftState = (liftState, None)

updateOnApproachingStorey :: State -> (State, ShouldStop)
updateOnApproachingStorey state@(State _storey S.Empty) =
  (state, StopAndStartBoardingCountdown)
updateOnApproachingStorey (State storey requests@(_ :|> requestNext)) =
  (State nextStorey requestsNew, shouldStop)
  where
    nextStorey
      | requestNext <= storey = storey
      | otherwise = storey + 1
    (requestsHere, requestsNew) = S.partition (== storey) requests
    shouldStop =
      if null requestsHere then NoStop else StopAndStartBoardingCountdown

updateOnUserRequest ::
  Integer -> State -> (State, Either Move StartBoardingCountdown)
-- just an optimization in case holding a button pressed can dispatch the same event repeatedly
updateOnUserRequest request state@(State _storey (latestRequest :<| _))
  | request == latestRequest = (state, Left None)
updateOnUserRequest request (State storey requests)
  | not (null requests) = (State storey (request :<| requests), Left None)
  | otherwise = -- lift is standing and receives its only request.
    -- so it is time to departure.
    second (\case {None -> Right StartBoardingCountdown; m -> Left m}) $
    updateOnDeparturingStorey $
    State storey (request :<| requests)

main :: IO ()
main =
  do
    state <- newIORef (State 0 mempty)
    forever $ do
      handleUserRequest state =<< readDepartureRequest
      handleUserRequest state =<< readDestinationRequest
      whenM readApproachingStorey $ do
        atomicModifyIORef' state updateOnApproachingStorey >>= \case
          StopAndStartBoardingCountdown -> stopMoving *> startBoardingCountdown
          NoStop -> pure ()
      whenM readBoardingCountdownDone $ do
        executeMove =<< atomicModifyIORef' state updateOnDeparturingStorey

handleUserRequest :: IORef State -> Maybe Integer -> IO ()
handleUserRequest _state Nothing = pure ()
handleUserRequest state (Just storey) =
  atomicModifyIORef' state (updateOnUserRequest storey) >>= \case
    Left move -> executeMove move
    Right StartBoardingCountdown -> startBoardingCountdown

executeMove :: Move -> IO ()
executeMove Down = moveDown
executeMove Up = moveUp
executeMove None = pure ()

-- primitives
readDepartureRequest :: IO (Maybe Integer)
readDepartureRequest = undefined

readDestinationRequest :: IO (Maybe Integer)
readDestinationRequest = undefined

readApproachingStorey :: IO Bool
readApproachingStorey = undefined

startBoardingCountdown :: IO ()
startBoardingCountdown = undefined

readBoardingCountdownDone :: IO Bool
readBoardingCountdownDone = undefined

stopMoving :: IO ()
stopMoving = undefined

moveDown :: IO ()
moveDown = undefined

moveUp :: IO ()
moveUp = undefined
